# Furniture store

This project is a furniture store. This project consists of a backend and a frontend parts.
Backend consists of a database(postgres), redis and celery (for mailing) and backend application.


## Preferences and Environment
### BACKEND 
>Create `.env` file in `./back/`. Example:
```
SECRET_KEY="your_key"
DEBUG=0
DJANGO_ALLOWED_HOSTS="*"
SQL_ENGINE=django.db.backends.postgresql
SQL_DATABASE=your_db
SQL_USER=your_db_user
SQL_PASSWORD=your_db_password
SQL_HOST=db
SQL_PORT=5432
EMAIL_HOST="smtp.gmail.com"
EMAIL_PORT=587
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST_USER="your@gmail.com"
EMAIL_HOST_PASSWORD="your_password"
CELERY_BROKER_URL="redis://redis:6379"
CELERY_RESULT_BACKEND="redis://redis:6379"
```

### DB
>Create `.env` file in `./db/`. Example:
```
POSTGRES_USER=your_db_user
POSTGRES_PASSWORD=your_db_password
POSTGRES_DB=your_db
```

### FRONTEND
>Create `.env` file in `./front/`. Example:
```
GMAPS_API_KEY=your_gmaps_key
BACKEND_HOST=back
BACKEND_PORT=8000
```

### Docker

- Install *docker*
- Install *docker-compose*


## Getting Start
1. Run command
```
docker-compose up --build -d
```

