const brand = {
  architect: {
    name: 'Кухни в дом',
    desc: 'Шоурум, где представлены образцы мебели',
    prefix: 'kitcheninhome',
    footerText: 'created by TopTeam ' + new Date().getFullYear(),
    logoText: 'Furniture MebHistory',
    projectName: '',
    footerProjectname: 'Кухни в дом',
    url: '/',
    img: '/static/images/furniture-logo.png',
  }
}

export default brand
