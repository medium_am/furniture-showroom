export default [
  {
    code: 'ru',
    iso: 'ru-RU',
    name: 'Русский',
    file: 'ru-RU.js',
    dir: 'ltr'
  }
]
